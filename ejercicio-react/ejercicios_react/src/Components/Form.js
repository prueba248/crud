import React from 'react'



const Form =({estudiante, setEstudiante})=>{


    const handleChange = e =>{
        setEstudiante({
            ...estudiante,
            [e.target.name]: e.target.value
        })
    }
    
    
    const handleSubmit=()=>{

        //1 OPERACION INSERTAR
        if(estudiante.id === '')
        {
                //validación de datos
                if(estudiante.nombre === ''|| estudiante.apellidos=== ''|| estudiante.carnet===''|| estudiante.cel===''){
                    alert('Campos obligatorios')
                    return
                }
                //consulta
                const requestInit={
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(estudiante)
                }
                fetch('http://localhost:9000/api', requestInit)
                .then(res=>res.text())
                .then(res=> console.log(res))
                    
                //reiniciando 
                setEstudiante({
                    id:'',
                    nombre:'',
                    apellidos:'',
                    carnet:'',
                    cel:''
                })
        }else if(estudiante.id !== ''){
            if(estudiante.nombre === ''|| estudiante.apellidos=== ''|| estudiante.carnet===''|| estudiante.cel===''){
                alert('Campos obligatorios')
                return
            }
            const requestInit = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(estudiante)
            }
            fetch('http://localhost:9000/api/' + estudiante.id, requestInit)
                .then(res => res.text())
                .then(res => console.log(res))


            //reiniciando  
            setEstudiante({
                id:'',
                nombre: '',
                apellidos: '',
                carnet: '',
                cel: ''
            })
        }

    }



    
    return(
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label htmlFor="nombre" className="form-label">Nombre</label>
                <input name="nombre" onChange={handleChange} type="text" id="nombre"  className="form-control" value={estudiante.nombre}/>
            </div>

            <div className="mb-3">
                <label htmlFor="apellidos" className="form-label">Apellidos</label>
                <input name="apellidos" onChange={handleChange} type="text" id="apellidos" className="form-control" value={estudiante.apellidos}/>
            </div>

            <div className="mb-3">
                <label htmlFor="carnet" className="form-label">Carnet</label>
                <input name="carnet" onChange={handleChange} type="text" id="carnet" className="form-control" value={estudiante.carnet}/>
            </div>

            <div className="mb-3">
                <label htmlFor="cel" className="form-label">Cel</label>
                <input name="cel" onChange={handleChange} type="text" id="cel" className="form-control" value={estudiante.cel}/>
            </div>
            <button type= "submit" className= "btn btn-primary">Enviar</button>
        </form>

    );

    

}
export default Form;