import React,{useState, useEffect} from 'react';

const List = ({ estudiante, setEstudiante, listEstu, setListUpdated, ListUpdated }) => {

    const handleDelete = id => {
        const requestInit = {
            method: 'DELETE'
        }
        fetch('http://localhost:9000/api/' + id, requestInit)
            .then(res => res.text())
            .then(res => console.log(res))
        setListUpdated(true)
    }

     let { nombre, apellidos, carnet, cel } = estudiante
    const handleUpdate = parametros => {
        let id = parametros.id;
        //validación de datos
        console.log('Datos de la fila' , parametros)
        setEstudiante({id: parametros.id, nombre: parametros.nombre, apellidos: parametros.apellidos, carnet: parametros.carnet, cel:parametros.cel})
              
        // if (nombre === '' || apellidos === '' || carnet === '' || cel === '') {
        //     alert('Campos obligatorios')
        //     return
        // }
        // const requestInit = {
        //     method: 'PUT',
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(estudiante)
        // }
        // fetch('http://localhost:9000/api/' + id, requestInit)
        //     .then(res => res.text())
        //     .then(res => console.log(res))


        // //reiniciando  
        // setEstudiante({
        //     nombre: '',
        //     apellidos: '',
        //     carnet: '',
        //     cel: ''
        // })

        // setListUpdated(true)

    }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Carnet</th>
                    <th>Cel</th>
                </tr>
            </thead>
            <tbody>

                {listEstu.map(listEstu => (
                    <tr key={listEstu.id}>
                        <td>{listEstu.id}</td>
                        <td>{listEstu.nombre}</td>
                        <td>{listEstu.apellidos}</td>
                        <td>{listEstu.carnet}</td>
                        <td>{listEstu.cel}</td>
                        <td>
                            <div className="mb-3">
                                <button onClick={() => handleDelete(listEstu.id)} className="btn btn-danger">Eliminar </button>
                            </div>
                        </td>
                        <td>
                            <div className="mb-3">
                                <button onClick={() => handleUpdate(listEstu)} className="btn btn-dark">Editar</button>
                            </div>
                          
                        </td>
                    </tr>
                ))}





            </tbody>
        </table>
    );

}

export default List;