import React, {Fragment, useState, useEffect} from 'react';
import Navbar from './Components/Navbar'
import List from './Components/studlist'
import Form from './Components/Form'


function App() {

  const [estudiante, setEstudiante]=useState({
    id:'',
    nombre:'',
    apellidos:'',
    carnet:'',
    cel:''
  })

  const [listEstu, setlistEstu]=useState([])
  const [listUpdated, setListUpdated]=useState(false)

 

  useEffect(()=>{
    const getlistEstu=()=>{
      fetch('http://localhost:9000/api')
      .then(res=>res.json())
      .then(res=> setlistEstu(res))
    }
    getlistEstu()
    setListUpdated(false)
  },[listUpdated])

  return (
   <Fragment>
     
     <Navbar brand='Estudiantes'/>
     <div className="container">
       <div className="row">
         <div className="col-7">
            <h2 style={{textAlign: 'center'}}>Lista de Estudiantes</h2>
            <List estudiante={estudiante} setEstudiante={setEstudiante} listEstu={listEstu} setListUpdated={setListUpdated}  />
         </div>
         <div className="col-5">
         <h2 style={{textAlign: 'center'}}>Formulario de Estudiantes</h2>
         <Form estudiante={estudiante} setEstudiante={setEstudiante}/>
         </div>
       </div>
     </div>
     
    </Fragment>
    
  );
}

export default App;
