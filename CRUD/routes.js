const express=require('express')
const routes= express.Router()

routes.get('/', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err)return res.send(err)
        conn.query('SELECT * FROM estu', (err, rows)=>{
            if(err) return res.send(err)

            res.json(rows)
        })
    })
})


routes.post('/', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err)return res.send(err)
        
        conn.query('INSERT INTO estu set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err)

            res.send('El estudiante ha sido registrado')
        })
    })
})


routes.delete('/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err)return res.send(err)
        
        conn.query('DELETE FROM estu WHERE id=?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('El estudiante ha sido eliminado')
        })
    })
})

routes.put('/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err)return res.send(err)
       
        conn.query('UPDATE estu set ? WHERE id=?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err)
            
            res.send('El estudiante ha sido actualizado')
        })
    })
})


module.exports=routes