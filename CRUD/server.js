const express = require('express')
const mysql=require('mysql')
const myconn=require('express-myconnection')
const cors=require('cors')

const routes=require('./routes')

 
const app = express()
app.set('port', process.env.PORT || 9000)
const dboptions={

    host:'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'estudiante'
}

//middlewares------------------------
app.use(myconn(mysql,dboptions, 'single'))
app.use(express.json())
app.use(cors())

//rutas------------------------------
app.get('/', function(req, res){
    res.send('Bienvenido')
})
app.use('/api', routes)

//corriendo el servidor-------------
app.listen(app.get('port'), function(){
    console.log('server corriendo en el puesto', app.get('port'))
});